# rivet-analysis

Introduction
--------
rivet is great for making histograms, but can be slow to rerun if you just want to make a small 
histogram change or play around with different variables and cuts. Thus, we are going to 
add printout to the rivet log file that will contain all the information we want from each event,
in a csv format. We could send this to another file as well (or directly to a csv file), but
this is just slightly simpler to code up, and e.g. we won't mix up which csv file matches to which 
rivet log file.

Getting output from rivet routine
--------

Inside your rivet routine, you'll need to add two lines using the `MSG_INFO` command. The first
will be inside `init()`, so it will print only once per time you run rivet. It will be the 
_header_ file for our csv file - so contain all the variable names. Because we'll be using these names
later, inside the data frame, we want them to be descriptive, concise, and contain no spaces (underscores are ok).


e.g. you'll be doing things accessing the variables with `df.var`, like this:
```
    cos_dphi = []
    for lphi,yphi in zip(df.lphi,df.yphi):
        cos_dphi += [math.cos(lphi-yphi)]
```
though it is possible to change a name once the csv file is loaded, or just open the csv file
and change the header by hand. 

You'll also need to be sure that your header contains the exact same variables in the exact same
order as your printout. This is important, or you'll have columns of numbers later but not know 
what they are!

nb: Make sure that either `MSG_INFO` statements are only used for the output variables, or 
you have some separate, specific text you'll place in each line so that you can separate
out other info messages from the output variables. One good way of treating other output
messages is to have them as `MSG_DEBUG`, and then you can turn on/off the extra debug output
by running rivet in debug mode (add `-l DEBUG` to the command, for example:
(`rivet -l DEBUG --analysis=myRivetRoutine -o myoutput.yoda myInputHepmcFile.hepmc &> log.rivet_example.out`)
and that way you don't end up with massive log files except when you're specifically debugging
something. I separate out my log messages with the string "data: ".

Inside the example rivet routine (note that you can just implement the two printouts into your
own rivet routine, no need to use this one!), you'll see the two `MSG_INFO` lines:

The first, inside `init()`, printing out the header (note that no matter what other variables you choose, it's important to 
print out the event weight!):

```
    MSG_INFO("data: weight,yz3,lz3,pTlep,pTgamma,ly_costhetastar,lydr,lydeta,lydphi,mly,jjdr,jjdeta,jjdphi,mjj,MET,MET_phi,lpt,lpz,leta,lphi,ypt,ypz,yeta,yphi,j1pt,j1pz,j1eta,j1phi,j2pt,j2pz,j2eta,j2phi");
```
and the second one is inside the analyze function, which runs once per event, and prints out all the variable values.

nb: If you're applying _cuts_ within the rivet routine, you'll want to make sure you place the printout 
such that you're printing out every single event you want to study later. You should place it after 
checks that ensure that each object you want in the event is present, but before any analysis-type
cuts that you might want to change later. You can always apply such cuts later, once we load the
csv file for our analysis!

Converting output to a csv file
--------

The file we'll be dealing with in this example is `log.rivet_example.out`, which already contains output from each event 
of all the variables we care about. When you run your own rivet routine, you'll probably want to give your log file a more descriptive name, so you'll be able to know what it came from later (e.g. the process, lepton flavour, any specifics you 
set within madgraph). It's important to stay organised!

To extract the csv file from the log, it's pretty simple - just grep for your keyword and pipe
the output into a csv file (recall I put "data" in every line of my log file,
and I know that this word was not anywhere else in the log):

```
  grep data log.rivet_example.out > rivet_example.csv #note i'm keeping the naming consistent here!
```

and then we want to remove the superfluous start to each line that messes with our nice csv format,
so just do a "sed" (command line find - replace) to find all instances of the string
`Rivet.Analysis.myRivetRoutine_EL: INFO  data: ` and replace it with nothing:

```
  on linux, you can use '-i', which means replace inplace in the file:
      sed -i 's/Rivet.Analysis.myRivetRoutine_MU: INFO  data: //g' rivet_example.csv
      
  on mac, you'll see a warning message that this does not work. instead, do the sed and then pipe back into the same file:
        sed 's/Rivet.Analysis.myRivetRoutine_MU: INFO  data: //g' rivet_example.csv > rivet_example.csv
```

If you've never used `sed` before, the bit that you're replacing goes between the `//` - here there's nothing between the forward slashes, but if you put `s/input/haha/g` it would replace all instances of the string "input" with "haha". 

If you have a lot of different hepmc files you've put through the same rivet routine, you 
can put the "grep" and "sed" commands into a bash script (`make_csv_files.sh`), and just run that
over all files matching your log naming pattern (in this example, I am running on files ending with
`_v01.out`, which you might name your first iteration with):

```
    #!/bin/bash
    FILES=($(ls -lht ~/EFT_Studies/rivet_routines/log.rivet_*v01.out | awk '{ print $NF}'))

    for i in "${FILES[@]}" 
    do
        fname=$(echo $i | cut -d'.' -f 2)
        echo $fname
        grep data $i > $fname.csv
        #only one of the following sed commands will do anything, but this covers
        #both muon and electron cases. If you only have one type of routine, then you only need one line.
        sed -i 's/Rivet.Analysis.myRivetRoutine_EL: INFO  data: //g' $fname.csv
        sed -i 's/Rivet.Analysis.myRivetRoutine_MU: INFO  data: //g' $fname.csv
    done
```

this is convenient if you've just made a small change to the rivet routine, rerun on _all_ 
your samples, and need to remake csv files for all the different inputs. 

the output file names are the same as the inputs, but without the `log.` and with `.csv` instead
of `.out`. This is achieved with the line `fname=$(echo $i | cut -d'.' -f 2)`, which splits the input file
name on the "." character, and returns the second field (note that indexing starts from 1).

Now you have a csv file, and we can start to look at this within the python notebook (`makeRivetPlots.ipynb`)!

Analysing the data
--------

## independent of cern version ##

If you _don't_ have a cern account, no problem. First, you'll need to install jupyter notebook. 
The easiest way to do that is with anaconda. See: https://www.dataquest.io/blog/jupyter-notebook-tutorial/.

## cern account version ##

If you have a cern account, clone this package somewhere into your eos directory, or copy over the notebook file via cernbox.cern.ch or on lxplus into `/eos/user/home-m/myusername/SomeWellNamedFolder/`.  Then, copy over the csv file you made into the same directory as the notebook. Now we can use swan.cern.ch as the  notebook server. 

Go to swan.cern.ch, and under "Software stack" select the first python3 stack. The other boxes are fine to leave as default for now. Click "start my session". This will redirect you to a page with Project, Share, and CERNBox. We put the files into our cernbox, so click cernbox and navigate to the directory with the notebook. Click then notebook, and it opens in a new tab. 


## using the notebook ##

If you've never used jupyter notebook before, first read the tutorial here: https://www.dataquest.io/blog/jupyter-notebook-tutorial/ and play around a little so you understand how the cell structure works and how to run code. 

Before you run anything, though, make sure to replace the file path in cell 4 to match where you put your csv files. Further instructions are inside the notebook!


ADVANCED

A limitation of the csv method/structure is that if you have an unknown number of objects but always want to 
print out them _all_, you'll first need to know the maximum number in order to have the correct
number of columns. Alternatively, you could implement this differently, so that some post-processing of the log
file could parse more lines of data to include N objects.

e.g. 
```
    for(unsigned int i=0; i < nJets; i++){
        MSG_INFO("data: jet " << i << ": " <<jets[i].pT() / GeV << "," << jets[i].pz() / GeV << "," << jets[i].eta() << "," << jets[i].phi() ");
    }
 ```   
and then you could concatenate the lines in a python script later.

